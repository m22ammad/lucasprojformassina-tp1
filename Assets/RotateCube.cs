using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public class RotateCube : MonoBehaviour
{   
    public float speedY = 3.0f;

    public InputAction activateRotation;
    public bool turn = false;
    // Start is called before the first frame update
    void Start()
    {
        activateRotation.Enable();
        activateRotation.started += cxt => turn = !turn;
    }


    // Update is called once per frame
    void Update()
    {
        if (turn){
        transform.Rotate(0,speedY,0);
        }
    }
}
